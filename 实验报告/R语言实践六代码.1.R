ADdata <- read.table("ADdata.txt") #读取实践3存储的文件
ADdata[ADdata == 0 ] <- NA #0替换成NA
ADdata <- log2(ADdata) #取对数
group <- colnames(ADdata) #提取样本名
group <- gsub("\\d","", group) #去掉数字字符，即为分组信息
testdata <- t(ADdata) #转置
p <- NULL #设置一个空的p对象
for( i in 1:ncol(testdata)) {
  value <- testdata[,i] #提取第i列 即第i个蛋白的数据
  pos1 = !is.na(value[group == "ad"]) #提取ad组不为NA的位置
  pos2 = !is.na(value[group == "asym"]) #提取asym组不为NA的位置
  pos3 = !is.na(value[group == "ctl"]) #提取ctl组不为NA的位置
  if( sum(pos1) < 3 | sum(pos2) < 3 | sum(pos3) < 3) {
    p <- c(p,NA) #当任意一个组不为NA的数<3 则p为NA
  } else { #否则进行方差分析
    aovtest <- oneway.test(value ~ group)
    p <- c(p,aovtest$p.value) #p值放入p对象
  }
}
stat <- data.frame(ID = colnames(testdata), pvalue = p) #建立数据框记录ID和P值
write.table(stat, file = "anova_test_result.txt")