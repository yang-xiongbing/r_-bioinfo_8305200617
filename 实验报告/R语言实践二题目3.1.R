#使用for语句计算num大于10的个数。
num=sample(1:50,10,replace = TRUE)
# for循环1到100的连加
ans <- 0               # 对结果赋初值
for (i in num) 
  if (i > 10){
  
  ans <- ans + 1       # 将每次的值累加
}
print(ans)

